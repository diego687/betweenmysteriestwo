﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRAMEWORK_GAME
{
    [CreateAssetMenu]
    public class StageData : ScriptableObject
    {
        public Stage _outcomeOne;
        public Stage _outcomeTwo;
        public Stage _outcomeThree;
    }
}