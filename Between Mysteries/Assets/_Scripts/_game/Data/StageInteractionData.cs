﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRAMEWORK_GAME
{
    [CreateAssetMenu]
    public class StageInteractionData : ScriptableObject
    {
        public AudioFile _interactionAudioSFX;
        public string _interactionText;
    }
}
