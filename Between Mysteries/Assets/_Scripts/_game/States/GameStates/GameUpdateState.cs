﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRAMEWORK_API;

namespace FRAMEWORK_GAME
{
    public class GameUpdateState : AbstractGameState
    {
        StageManager _stageManager;

        public override void OnEnter()
        {
            base.OnEnter();
            _stageManager = Game.Inst.GetStageManager();
            //Debug.Log("On enter game update state");
        }

        public override void OnUpdate()
        {
            base.OnUpdate();
            //Debug.Log("Update of GameUpdateState");
            if (Game.Inst.GetActiveStage().GetStageFinished()) // Fix: maybe with a trigger from Game to update this once the stage is done.
            {
                _stageManager.SpawnStage(Game.Inst.GetActiveStage().GetNextStage());
            }
        }

        public override void OnExit()
        {
            // Logic goes here
            base.OnExit();
        }
    }
}