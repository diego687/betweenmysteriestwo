﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRAMEWORK_API;

namespace FRAMEWORK_GAME
{
    // This is the first State of the game
    public class GameLoadingState : AbstractGameState
    {
        public override void OnEnter()
        {
            base.OnEnter();
            //Debug.Log("On enter game loading state");
            Game.Inst.LoadVariables();
            Game.Inst.Initialize();
            // Load Resources needed for the game
            Game.Inst.LoadResources();
            // Start the game
            Game.Inst.SetState(new GameEnterState());
        }

        public override void OnUpdate()
        {
            base.OnUpdate();
        }

        public override void OnExit()
        {
            //Debug.Log("On exit game loading state");
            base.OnExit();
        }
    }
}