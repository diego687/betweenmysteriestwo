﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRAMEWORK_API;

namespace FRAMEWORK_GAME
{
    public class GameExitState : AbstractGameState
    {
        public override void OnEnter()
        {
            base.OnEnter();
            //Debug.Log("On Enter game exit state");
        }

        public override void OnUpdate()
        {
            base.OnUpdate();
        }

        public override void OnExit()
        {
            // Logic here

            base.OnExit();
        }
    }
}