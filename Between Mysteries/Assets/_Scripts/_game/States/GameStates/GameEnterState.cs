﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRAMEWORK_API;

namespace FRAMEWORK_GAME
{
    public class GameEnterState : AbstractGameState
    {
        public override void OnEnter()
        {
            base.OnEnter();
            //Debug.Log("OnEnter Game enter state");
            Game.Inst.GetStageManager().Initialize();
            Game.Inst.GetChapterManager().Initialize();
            // Exit the stage and go to update
            Game.Inst.SetState(new GameUpdateState());
        }

        public override void OnUpdate()
        {
            base.OnUpdate();
            //Debug.Log("game enter state update");
        }

        public override void OnExit()
        {
            //Debug.Log("On exit game enter state");
            base.OnExit(); // this should be at the end so it's removed once the state logic is done
        }
    }
}