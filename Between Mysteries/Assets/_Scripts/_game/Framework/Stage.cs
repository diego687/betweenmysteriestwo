﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRAMEWORK_API;

namespace FRAMEWORK_GAME
{
    // Note: create interface with initialize and destroy methods. Should we add button functions to the interface?
    public class Stage : MonoBehaviour
    {
        public Coroutine _fadingSpriteCoroutine;
        public Coroutine _fadingImageCoroutine;
        public Coroutine _lerpingObjectCoroutine;
        public Coroutine _scaleObjectCoroutine;
        public bool _stageFinished;
        public StageData _stageData;
        public Button[] _stageButtons;
        public bool _waitingForInput;
        public string _upcommingStage;
        //public Collider2D[] _stageInteractionButtonsArray;
        //List<Collider2D> _stageInteractionButtonsList;

        string _nextStage;
        List<UnityEngine.Events.UnityAction> _buttonActions;
        AudioManager _audioManager;
        TransitionManager _transitionManager;
        int _stageID;
        Canvas _stageCanvas;

        void Start()
        {
        
        }

        void Update()
        {

        }

        public void SetStageID(int ID)
        {
            _stageID = ID;
        }

        public int GetStageID()
        {
            return _stageID;
        }

        public void SetStageFinished()
        {
            _stageFinished = true;
            CSceneLoader.Inst._fadeOut = true;
            if (GetNextStage() == null)
                SetNextStage(GetStageData()._outcomeOne.name); // this gets sets in every stage
                                                               // if that stage didn't specify a stage to follow, it will be the first outcome by default
        }

        public bool GetStageFinished()
        {
            return _stageFinished;
        }

        public void SetWaitingForInput(bool aValue)
        {
            _waitingForInput = aValue;
        }

        public bool GetWaitingForInput()
        {
            return _waitingForInput;
        }

        public virtual void Initialize()
        {
            // check if this goes here or in initialize
            _stageFinished = false; // set stage finished for future scenes and for the stage tester
            _audioManager = AudioManager.Inst;
            _transitionManager = TransitionManager.Inst;
            LoadStageButtons();
        }

        //public void LoadStageInteractions()
        //{
        //    _stageInteractionButtonsArray = GetComponentsInChildren<Collider2D>();
        //    if (_stageInteractionButtonsArray.Length > 0)
        //    {
                
        //    }
        //}

        public void LoadCanvas()
        {
            _stageCanvas = GetComponentInChildren<Canvas>();
            if (_stageCanvas != null)
            {
                _stageCanvas.worldCamera = Game.Inst.GetCamera();
                _stageCanvas.sortingLayerName = "UI";
            }                
        }

        public void LoadStageButtons()
        {
            _stageButtons = GetComponentsInChildren<Button>();
            //Debug.Log("Stage buttons lenght: " + _stageButtons.Length);
            if (_stageButtons.Length > 0)
            {
                LoadCanvas();
                _waitingForInput = true;
                AddUnityActions();
                for (int i = 0; i < _stageButtons.Length; i++)
                {
                    _stageButtons[i].onClick.AddListener(_buttonActions[i]);
                }
            }
        }

        public void AddUnityActions()
        {
            _buttonActions = new List<UnityEngine.Events.UnityAction>();
            _buttonActions.Add(OnFirstButtonClicked);
            _buttonActions.Add(OnSecondButtonClicked);
            _buttonActions.Add(OnThirdButtonClicked);
            _buttonActions.Add(OnFourthButtonClicked);
            _buttonActions.Add(OnFifthButtonClicked);
        }

        /// <summary>
        // This should return the next possible outcome in each stage depending on the behaviour of that button
        /// This can be a Stage, a dialogue or anything that is expected to happen once that button is clicked
        /// </summary>
        public virtual void OnFirstButtonClicked()
        {
            Debug.Log("first clicked button ");
            _waitingForInput = false;
        }

        /// <summary>
        // This should return the next possible outcome in each stage depending on the behaviour of that button
        /// This can be a Stage, a dialogue or anything that is expected to happen once that button is clicked
        /// </summary>
        public virtual void OnSecondButtonClicked()
        {
            Debug.Log("second button clicked");
            _waitingForInput = false;
        }

        /// <summary>
        // This should return the next possible outcome in each stage depending on the behaviour of that button
        /// This can be a Stage, a dialogue or anything that is expected to happen once that button is clicked
        /// </summary>
        public virtual void OnThirdButtonClicked()
        {
            //Debug.Log("Third button clicked");
            _waitingForInput = false;
        }

        /// <summary>
        // This should return the next possible outcome in each stage depending on the behaviour of that button
        /// This can be a Stage, a dialogue or anything that is expected to happen once that button is clicked
        /// </summary>
        public virtual void OnFourthButtonClicked()
        {
            //Debug.Log("Third button clicked");
            _waitingForInput = false;
        }

        /// <summary>
        // This should return the next possible outcome in each stage depending on the behaviour of that button
        /// This can be a Stage, a dialogue or anything that is expected to happen once that button is clicked
        /// </summary>
        public virtual void OnFifthButtonClicked()
        {
            //Debug.Log("Third button clicked");
            _waitingForInput = false;
        }

        public virtual void DestroyStage()
        {
            Destroy(this.gameObject);
        }

        public AudioManager GetAudioManager()
        {
            return _audioManager;
        }

        public TransitionManager GetTransitionManager()
        {
            return _transitionManager;
        }

        public Coroutine FadingToBlack()
        {
            return _transitionManager._fadeCoroutine;
        }

        public StageData GetStageData()
        {
            return _stageData;
        }

        public void SetNextStage(string aStageName)
        {
            _nextStage = aStageName;
        }

        public string GetNextStage()
        {
            return _nextStage;
        }

        public virtual Coroutine FadeSprite(SpriteRenderer aRenderer, bool aFadeIn, float aTime)
        {
            return _fadingSpriteCoroutine = StartCoroutine(FadeSpriteCoroutine(aRenderer, aFadeIn, aTime));
        }

        IEnumerator FadeSpriteCoroutine(SpriteRenderer aRenderer, bool aFadeIn, float aTime)
        {
            float startTime = Time.time;
            while (Time.time - startTime < aTime)
            {
                float t = (Time.time - startTime) / aTime;
                aRenderer.color = aRenderer.color.withAlpha(aFadeIn ? t : 1 - t);
                yield return null;
            }

            aRenderer.color = aRenderer.color.withAlpha(aFadeIn ? 1 : 0);
            _fadingSpriteCoroutine = null;
        }

        public virtual Coroutine FadeImage(Image aImage, bool aFadeIn, float aTime)
        {
            return _fadingImageCoroutine = StartCoroutine(FadeImageCoroutine(aImage, aFadeIn, aTime));
        }

        IEnumerator FadeImageCoroutine(Image aImage, bool aFadeIn, float aTime)
        {
            float startTime = Time.time;
            while (Time.time - startTime < aTime)
            {
                float t = (Time.time - startTime) / aTime;
                aImage.color = aImage.color.withAlpha(aFadeIn ? t : 1 - t);
                yield return null;
            }

            aImage.color = aImage.color.withAlpha(aFadeIn ? 1 : 0);
            _fadingImageCoroutine = null;
        }

        public virtual Coroutine LerpObject(GameObject aGmeObject, Vector3 aOrigin, Vector3 aDestination, float aTime = 1)
        {
            return _lerpingObjectCoroutine = StartCoroutine(LerpGameObjectCoroutine(aGmeObject, aOrigin, aDestination, aTime));
        }

        IEnumerator LerpGameObjectCoroutine(GameObject aGameObject, Vector3 aOrigin, Vector3 aDestination, float aTime)
        {
            float startTime = Time.time;
            while (Time.time - startTime < aTime)
            {
                float t = (Time.time - startTime) / aTime;
                aGameObject.transform.position = Vector3.Lerp(aOrigin, aDestination, t);
                yield return null;
            }

            aGameObject.transform.position = aDestination;
            _lerpingObjectCoroutine = null;
        }

        public virtual Coroutine ScaleGameObject(GameObject aGameObject, Vector3 aOrigin, Vector3 aDesiredScale, float aTime)
        {
            return _scaleObjectCoroutine = StartCoroutine(ScaleObjectCoroutine(aGameObject, aOrigin, aDesiredScale, aTime));
        }

        IEnumerator ScaleObjectCoroutine(GameObject aObject, Vector3 aOriginalScale, Vector3 aDesiredScale, float aTime)
        {
            float startTime = Time.time;
            while (Time.time - startTime < aTime)
            {
                float t = (Time.time - startTime) / aTime;
                aObject.transform.localScale = Vector3.Lerp(aOriginalScale, aDesiredScale, t);
                yield return null;
            }

            aObject.transform.localScale = aDesiredScale;
            _scaleObjectCoroutine = null;
        }
    }
}