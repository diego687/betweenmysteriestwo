﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRAMEWORK_API;

namespace FRAMEWORK_GAME
{
    public class Game : Singleton<Game>
    {
        // Debug
        public int _startingChapter;
        // State management
        AbstractGameState _currentState;
        // Entities
        StageManager _stageManager;
        ChapterManager _chapterManager;
        Stage _activeStage;
        Chapter _activeChapter;
        TransitionManager _transitionManager;
        AudioManager _audioManager;
        ShakeManager _shakeManager;
        Camera _gameCamera;

        void Start()
        {
            SetState(new GameLoadingState());
        }

        public void Initialize()
        {
            _stageManager = StageManager.Inst;
            _transitionManager = TransitionManager.Inst;
            _audioManager = AudioManager.Inst;
            _chapterManager = ChapterManager.Inst;
            _shakeManager = ShakeManager.Inst;
            _gameCamera = Camera.main;
        }

        void Update()
        {

        }

        public void SetState(AbstractGameState aState)
        {
            if (_currentState != null)
                _currentState.OnExit();
            _currentState = aState;
            _currentState.OnEnter();
        }

        public void LoadVariables()
        {
            
        }

        /// <summary>
        /// Load resources for each array or data a class needs
        /// </summary>
        public void LoadResources()
        {
            _stageManager.SetStageDataArray(Resources.LoadAll<StageData>("Data/Stages"));
            _stageManager.SetStageArray(Resources.LoadAll<Stage>("Prefabs/Stages"));
            _chapterManager.SetChapterArray(Resources.LoadAll<Chapter>("Prefabs/Chapters"));
        }

        public StageManager GetStageManager()
        {
            return _stageManager;
        }

        public ChapterManager GetChapterManager()
        {
            return _chapterManager;
        }

        public void FadeTransitionToBlack(bool aFade)
        {
         //   _transitionManager.fadeScreenToBlack(aFade);
        }

        public Coroutine FadingToBlack()
        {
            return _transitionManager._fadeCoroutine;
        }

        public AudioManager GetAudioManager()
        {
            return _audioManager;
        }

        public ShakeManager GetShakeManager()
        {
            return _shakeManager;
        }

        public void ShakeGameObject(GameObject aGameObject, float aIntensity, float aDuration, float aSmoothness, bool aInfinite = false)
        {
            _shakeManager.shakeGameObject(aGameObject, aIntensity, aDuration, aSmoothness, aInfinite);
        }

        public void StopShakes()
        {
            _shakeManager.StopShake();
        }

        public void StopInfiniteShakes()
        {
            Debug.Log("game infinite shakes");
            _shakeManager.StopInfiniteShakes();
        }

        public Coroutine Shaking()
        {
            return _shakeManager._shaking;
        }

        public void SetActiveStage(Stage aStage)
        {
            if (_activeStage != null)
                _activeStage.DestroyStage();
            _activeStage = aStage;
            _activeStage.Initialize();
        }

        public Stage GetActiveStage()
        {
            return _activeStage;
        }

        public Camera GetCamera()
        {
            return _gameCamera;
        }

        public void SetActiveChapter(Chapter aChapter)
        {
            if (_activeChapter != null)
                _activeChapter.DestroyChapter();
            _activeChapter = aChapter;
            // Init chapter;
        }

        public Chapter GetActiveChapter()
        {
            return _activeChapter;
        }
    }
}
