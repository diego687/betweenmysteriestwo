﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRAMEWORK_GAME
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class HiddenObject : MonoBehaviour
    {
        Coroutine _lerping;
        BoxCollider2D _collider;
        bool _clicked;

        void Start()
        {
            _collider = GetComponent<BoxCollider2D>();
        }

        void Update()
        {

        }

        public Coroutine Lerp(Vector3 aOrigin, Vector3 aDestination, float aTime)
        {
            return _lerping = StartCoroutine(LerpCoroutine(aOrigin, aDestination, aTime));
        }

        IEnumerator LerpCoroutine(Vector3 aOrigin, Vector3 aDestination, float aTime)
        {
            float startTime = Time.deltaTime;
            while (Time.time - startTime < aTime)
            {
                float t = (Time.time - startTime) / aTime;
                transform.position = Vector3.Lerp(aOrigin, aDestination, t);
                yield return null;
            }

            transform.position = aDestination;
            _lerping = null;
        }

        public bool IsLerping()
        {
            return _lerping != null;
        }

        public void EnableCollider(bool aValue)
        {
            _collider.enabled = aValue;
        }

        public bool GetClicked()
        {
            return _clicked;
        }

        public void OnMouseDown()
        {
            //Debug.Log("On mouse down hidden object");
            _clicked = true;
        }

        //public void OnMouseUp()
        //{
        //    //Debug.Log("On mouse up hidden object");
        //}
    }
}