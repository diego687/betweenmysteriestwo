﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRAMEWORK_API;

namespace FRAMEWORK_GAME
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class StageInteraction : MonoBehaviour
    {
        public StageInteractionData _interactionData;
        public bool _interactionPressed;
        Collider2D _collider;

        void Start()
        {
            _collider = GetComponent<Collider2D>();
            _collider.enabled = true;
        }

        void Update()
        {

        }

        private void OnMouseDown()
        {
            Debug.Log(name + " was pressed");
            _interactionPressed = true;
            if (_interactionData != null)
            {
                if (_interactionData._interactionAudioSFX != null)
                    AudioManager.Inst.PlaySFX(_interactionData._interactionAudioSFX.name, false);
                Debug.Log(_interactionData._interactionText);
            }
            _collider.enabled = false;
        }

        private void OnMouseUp()
        {

        }
    }
}