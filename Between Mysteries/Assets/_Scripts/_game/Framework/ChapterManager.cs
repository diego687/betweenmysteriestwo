﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRAMEWORK_API;

namespace FRAMEWORK_GAME
{
    public class ChapterManager : Singleton<ChapterManager>
    {
        Chapter[] _chapterArray;
        Dictionary<string, Chapter> _chapterDictionary;

        protected override void Awake()
        {
            base.Awake();
            _chapterDictionary = new Dictionary<string, Chapter>();
        }

        public void Initialize()
        {
            LoadDictionary();
            SpawnChapter(_chapterArray[0].name);
        }

        void Start()
        {

        }

        void Update()
        {

        }

        void LoadDictionary()
        {
            for (int i = 0; i < _chapterArray.Length; i++)
            {
                _chapterDictionary.Add(_chapterArray[i].name, _chapterArray[i]);
            }
        }

        public void SetChapterArray(Chapter[] aChapterArray)
        {
            _chapterArray = aChapterArray;
        }

        public void SpawnChapter(string aChapterName)
        {
            Chapter tempChapter = Instantiate(_chapterDictionary[aChapterName]);
            Game.Inst.SetActiveChapter(tempChapter);
        }
    }
}