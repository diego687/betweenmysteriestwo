﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRAMEWORK_API;

// Add dictionary for stage data
namespace FRAMEWORK_GAME
{
    public class StageManager : Singleton<StageManager>
    {
        // Public
        public Stage[] _stageArray;
        // Private
        StageData[] _stageDataArray;
        Dictionary<string, Stage> _stageDictionary;

        protected override void Awake()
        {
            base.Awake();
            _stageDictionary = new Dictionary<string, Stage>();
        }

        void Start()
        {

        }

        public void Initialize()
        {
            LoadStageDictionary();
            //SpawnStageWithID(1);
            SpawnStage(_stageArray[Game.Inst._startingChapter].name); // For now, we leave this with one so it starts on the first scene 
                                             // this will later change so it starts on the stage marked on the save file
        }

        void Update()
        {
            #region Debug
            if (Input.GetKeyDown(KeyCode.D))
                Debug.Log("stage count; " + _stageArray.Length + " stage data count; " + _stageDataArray.Length);
            #endregion
        }

        void LoadStageDictionary()
        {
            for (int i = 0; i < _stageArray.Length; i++)
            {
                _stageDictionary.Add(_stageArray[i].name, _stageArray[i]);
            }
        }

        public void SetStageDataArray(StageData[] aStageDataArray)
        {
            _stageDataArray = aStageDataArray;
        }

        public void SetStageArray(Stage[] aStageArray)
        {
            _stageArray = aStageArray;
        }

        /// <summary>
        /// this is only for debug
        /// When starting the game from a save file, this will be removed. SpawnStage will be used instead with the name of the stage to spawn as a string
        /// </summary>
        /// <param name="id"></param>
        public void SpawnStageWithID(int id)
        {
            Stage tempStage = Instantiate(_stageArray[id]);
            tempStage.transform.position = Vector3.zero;
            tempStage.SetStageID(id);
            //Debug.Log("stage ID: " + tempStage.GetStageID());
            Game.Inst.SetActiveStage(tempStage);
        }

        public void SpawnStage(string aStageName)
        {
            Stage tempStage = Instantiate(_stageDictionary[aStageName]); // TryGetValue may be best here
            tempStage.transform.position = Vector3.zero;
            //Debug.Log("Stage Created With Dictionary");
            Game.Inst.SetActiveStage(tempStage);
        }
    }
}