﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CSceneLoader : MonoBehaviour
{
    private AsyncOperation _loader;
    private string _name;
    private int _sceneLoadType;

    public static CSceneLoader Inst;
    [HideInInspector]
    public bool _loadingScene = false;
    [HideInInspector]
    public string _currentScene;
    [HideInInspector]
    public string _PreviousCurrentScene;
    public bool _fadeOut;
    void Awake()
    {
        Inst = this;
        DontDestroyOnLoad(this.gameObject);
    }
    void Update()
    {

        if (_loader == null && CTransitionsManager.Inst.IsScreenCovered())
        {
            //if (_sceneLoadType == 0)
            //{
            //    _loader = SceneManager.LoadSceneAsync(_name);
            //}
            //else if (_sceneLoadType == 1)
            //{
            //    _loader = SceneManager.UnloadSceneAsync(_name);
            //}
            //else if(_sceneLoadType == 2)
            //{

            //}
            _fadeOut = true;
        }

        if (_loader != null )
        {
            if (_loader.progress >= 1)
            {
                CTransitionsManager.Inst.FadeOut();
                _loader = null;
            }
        }
        else if (_fadeOut)
        {
            CTransitionsManager.Inst.FadeOut();
            _fadeOut = false;
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            //CSceneLoader.Inst.LoadScene("Prueba cambio escenas", TransitionType.CIRCULAR_INOUT);
            CEffectManager.Inst.AddEvent(EffectType.UP_AND_DOWN);
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            //CSceneLoader.Inst.LoadScene("Prueba cambio escenas", TransitionType.CIRCULAR_INOUT);
            CEffectManager.Inst.returnToNormalView(EffectType.UP_AND_DOWN);
        }
    }
    /// <summary>
    /// This function makes the transition between scenes when Loading. Name = name of the scene you want to load. textureFirstTransition = the first shape of black-white transition you want. textureSecondTransition = second shape transition.
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="textureFirstTransition"></param>
    /// <param name="textureSecondTransition"></param>
    public void LoadScene(string Name, TransitionType aType = TransitionType.UP_AND_DOWN)
    {
        _PreviousCurrentScene = _name;
        _name = Name;
        _sceneLoadType = 0;
        _currentScene = Name;
        _loadingScene = true;
        CTransitionsManager.Inst.AddEvent(aType);
    }
    /// <summary>
    /// This function makes the transition between scenes when UnLoadig. Name = name of the scene you want to load. textureFirstTransition = the first shape of black-white transition you want. textureSecondTransition = second shape transition.
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="textureFirstTransition"></param>
    /// <param name="textureSecondTransition"></param>
    public void UnLoadScene(string Name, TransitionType aType = TransitionType.UP_AND_DOWN)
    {
        _name = Name;
        _sceneLoadType = 1;
        _loadingScene = false;
        CTransitionsManager.Inst.AddEvent(aType);
    }
    public void loadStage(TransitionType aType = TransitionType.UP_AND_DOWN)
    {
        _sceneLoadType = 2;
        //_name = Name;
        CTransitionsManager.Inst.AddEvent(aType);
    }
    public string getSceneName()
    {
        return _name;
    }
}
