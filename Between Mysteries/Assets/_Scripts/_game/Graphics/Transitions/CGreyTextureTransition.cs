﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor
using UnityEngine;
using UnityEngine.SceneManagement;

public class CGreyTextureTransition : CTransition
{
    private float _time = 1;
    public Material _mat;
    private Shader _shader;

    public Texture _greyScaleTexture;
    public Texture _shapeTexture;

    private float _scaleTime;
    private Coroutine _startTransitionCorroutine;
    private Coroutine _endTransitionOnLoad;
    public static CGreyTextureTransition _inst;
    private bool _startTransition = true;
    private bool _startTransitionEnded = false;
    private CSceneLoader _sceneLoader;
    // Use this for initialization
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
    void Start()
    {
        _inst = this;
        _shader = _mat.shader;
        _sceneLoader = CSceneLoader.Inst;
    }

    public override void UpdateTransition()
    {
        if (_startTransition)
        {
            initializeTransition(1.5f, _shapeTexture , _greyScaleTexture);
            _startTransition = false;
        }

        else if (GetState() == STATE_FADEOUT && _startTransitionEnded)
        {
            TransitionGameLoaded(1.5f, _shapeTexture, _greyScaleTexture);
        }
    }

    //This CoRoutine controls the transition between mini games
    public void initializeTransition(float lerpTime, Texture ShapeTexture, Texture grayScale_Texture)
    {
        _scaleTime = lerpTime;
        _mat.SetTexture("_GreyTexture", grayScale_Texture );
        _mat.SetTexture("_ShapeTexture", ShapeTexture);
        _startTransitionCorroutine = StartCoroutine(LoadingTransition());
    }

    private IEnumerator LoadingTransition()
    {
        yield return StartCoroutine(startTransition());
        //yield return new WaitForSeconds(.5f);
        _startTransitionEnded = true;
        SetState(STATE_HOLD);
        _startTransitionCorroutine = null;
    }

    private IEnumerator startTransition()
    {
        float elapsed = 0f;
        float time = _scaleTime;
        while ((elapsed <= time))
        {
            {
                elapsed += Time.deltaTime;
                float t = elapsed / time;
                _mat.SetFloat("_Cutoff", Mathf.Lerp(0, 1, t));
                yield return new WaitForEndOfFrame();
            }
            yield return null;
        }
    }

    public void TransitionGameLoaded(float lerpTime, Texture ShapeTexture, Texture grayScale_Texture)
    {
        _scaleTime = lerpTime;
        _mat.SetTexture("_GreyTexture", grayScale_Texture);
        _mat.SetTexture("_ShapeTexture", ShapeTexture );
        _endTransitionOnLoad = StartCoroutine(GameLoadedTransition());
    }

   
    private IEnumerator GameLoadedTransition()
    {
        yield return StartCoroutine(endTransition());
    }

    private IEnumerator endTransition()
    {
        float elapsed = 0f;
        float time = _scaleTime;
        _startTransitionEnded = false;
        yield return new WaitForSeconds(.3f);
        while ((elapsed < time))
        {
            {
                elapsed += Time.deltaTime;
                float t = elapsed / time;
                _mat.SetFloat("_Cutoff", Mathf.Lerp(1, 0, t));
                yield return new WaitForEndOfFrame();
            }
        }
        SetState(STATE_FADEIN);
        SetAsFinished();
        _endTransitionOnLoad = null;
        yield return null;
    }


}
