﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CTransitionsManager : MonoBehaviour
{

    public static CTransitionsManager Inst;
    private List<CTransition> _transitions;
    private GameObject[] _eventObjects;
   // public Transform _canvas;
    private bool _fadeOutFlag = false;
    private CTransition _currentTransition;

    void Awake()
    {
        Inst = this;
        _transitions = new List<CTransition>();
        DontDestroyOnLoad(this.gameObject);
        _eventObjects = Resources.LoadAll<GameObject>("Transitions/");
    }
    private void Start()
    {

    }
    public void AddEvent(TransitionType type)
    {
        for (int i = 0; i < _eventObjects.Length; i++)
        {
            if (_eventObjects[i].GetComponent<CTransition>()._type == type)
            {
               // _canvas.gameObject.SetActive(true);
                GameObject obj = Instantiate<GameObject>(_eventObjects[i], Vector3.zero, _eventObjects[i].transform.localRotation);
                _currentTransition = obj.GetComponent<CTransition>();
                _fadeOutFlag = false;
                obj.transform.localPosition = Vector3.zero;
                _transitions.Add(obj.GetComponent<CTransition>());
                break;
            }
        }
    }

    void Update()
    {
        for (int i = _transitions.Count - 1; i >= 0; i--)
        {
            _transitions[i].UpdateTransition();
            if (_transitions[i].Finished())
            {
                _transitions[i].Cleanup();
                Destroy(_transitions[i].gameObject);
                _transitions.RemoveAt(i);
            }
        }
        if (_currentTransition != null)
        {
            if (_currentTransition.GetState() == CTransition.STATE_HOLD)
            {
                if (_fadeOutFlag)
                {
                    _currentTransition.SetState(CTransition.STATE_FADEOUT);
                    _fadeOutFlag = false;
                }
            }

            if (_currentTransition.Finished())
            {
                Destroy(_currentTransition.gameObject);
                _currentTransition = null;
            }
        }
    }

    public bool IsScreenCovered()
    {
        return _currentTransition != null && _currentTransition.GetState() == CTransition.STATE_HOLD;
    }

    public void FadeOut()
    {
        _fadeOutFlag = true;
    }

    public CTransition getCurrentTransition()
    {
        return _currentTransition;
    }

}

public enum TransitionType
{
    UP_AND_DOWN,
    LEFT_TO_RIGHT,
    RIGHT_TO_LEFT,
    TORNADO,
    SMOKE,
    POKEMON_STYLE,
    UP_TO_DOWN,
    DOWN_TO_UP,
    PUZZLE_PIECES,
    CIRCULAR_INOUT,
    SMOKE_INK,
    DIAGONAL_INOUT,
    DIAGONAL_OUTIN,
    PENCIL_STYLE,

}