﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CEffectBlitter : MonoBehaviour
{
    public Material _mat;
    public void Start()
    {
        if (CTransitionsManager.Inst.getCurrentTransition() == null)
        {
            _mat.SetFloat("_Cutoff", 0);
        }
    }
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, _mat);
    }
}
