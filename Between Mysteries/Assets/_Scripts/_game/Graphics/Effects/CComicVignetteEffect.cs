﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor
using UnityEngine;
using UnityEngine.SceneManagement;

public class CComicVignetteEffect : CEffect
{
    private bool _barrsOnMiddle = false;
    public Material _mat;
    public Texture _greyScaleTexture;
   // public Texture _shapeTexture;
    public float _scaleTime;

    public static CComicVignetteEffect _inst;
    private bool _startEffect = false;

    public float _screenToCover;
    public float _speed;
    CEffectManager _manager;

    public GameObject Barr1_object;
    public GameObject Barr2_object;

    public Vector3 Barr1_mediumPos;
    public Vector3 Barr1_FinalPos;

    public Vector3 Barr2_mediumPos;
    public Vector3 Barr2_FinalPos;

    private Vector3 Barr1_originalPos;
    private Vector3 Barr2_originalPos;

    // Use this for initialization
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
    void Start()
    {
        _mat.SetFloat("_Cutoff", 0);
        _inst = this;
        _mat.SetTexture("_TransitionTex", _greyScaleTexture);
       // _mat.SetTexture("_ShapeTexture", _shapeTexture);
        _manager = CEffectManager.Inst;

        Barr1_originalPos = Barr1_object.transform.localPosition;
        Barr2_originalPos = Barr2_object.transform.localPosition;

        Barr1_object = Instantiate(Barr1_object);
        Barr2_object = Instantiate(Barr2_object);
    }

    public override void UpdateEffect()
    {
        if(_barrsOnMiddle == false)
        {
            StartCoroutine(moveBarrsToMiddle());
            _barrsOnMiddle = true;
        }
        else if (_startEffect && _barrsOnMiddle == true)
        {
            StartCoroutine(startTransition());
            _startEffect = false;
        }

        else if (_end == true)
        {
            StartCoroutine(ReturnToNormal());
            _end = false;
        }
    }

    private IEnumerator moveBarrsToMiddle()
    {
        float elapsed = 0f;
        float time = _scaleTime;
        while ((elapsed <= time))
        {
        
            elapsed += Time.deltaTime * _speed*10;
            float t = elapsed / time;
            Barr1_object.transform.localPosition = Vector3.Lerp(Barr1_originalPos, Barr1_mediumPos, t);
            Barr2_object.transform.localPosition = Vector3.Lerp(Barr2_originalPos, Barr2_mediumPos, t);
            yield return new WaitForEndOfFrame();
            
        }
        _startEffect = true;
        yield return null;

    }

    private IEnumerator startTransition()
    {
        float elapsed = 0f;
        float time = _scaleTime;
        while ((elapsed <= time))
        {
            {
                elapsed += Time.deltaTime* _speed;
                float t = elapsed / time;
                Barr1_object.transform.position =  Vector3.Lerp(Barr1_mediumPos, Barr1_FinalPos, t*1f);
                Barr2_object.transform.position = Vector3.Lerp(Barr2_mediumPos, Barr2_FinalPos, t*1f);
                _mat.SetFloat("_Cutoff", Mathfx.Coserp(0, _screenToCover, t));
                yield return new WaitForEndOfFrame();
            }
        }
        yield return null;

    }

    private IEnumerator ReturnToNormal()
    {

        _mat.SetFloat("_Cutoff", 0);
        Destroy(Barr1_object);
        Destroy(Barr2_object);
        SetAsFinished();
        yield return null;
    }


}
