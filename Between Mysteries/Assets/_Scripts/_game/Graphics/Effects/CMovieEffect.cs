﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor
using UnityEngine;
using UnityEngine.SceneManagement;

public class CMovieEffect : CEffect
{
    public Material _mat;
    public Texture _greyScaleTexture;
    public Texture _shapeTexture;
    public float _scaleTime;

    public static CMovieEffect _inst;
    private bool _startEffect = true;
    private bool _returnToNormal = false;

    public float _screenToCover;
    public float _speed;
    CEffectManager _manager;
    // Use this for initialization
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
    void Start()
    {
        _inst = this;
        _mat.SetTexture("_GreyTexture", _greyScaleTexture);
        _mat.SetTexture("_ShapeTexture", _shapeTexture);
        _manager = CEffectManager.Inst;
    }

    public override void UpdateEffect()
    {
        if (_startEffect)
        {
            StartCoroutine(startTransition());
            _startEffect = false;
        }

        else if (_end)
        {
            StartCoroutine(ReturnToNormal());
            _end = false;
        }
    }

    private IEnumerator startTransition()
    {
        float elapsed = 0f;
        float time = _scaleTime;
        while ((elapsed <= time))
        {
            {
                elapsed += Time.deltaTime* _speed;
                float t = elapsed / time;
                _mat.SetFloat("_Cutoff", Mathfx.Sinerp(0, _screenToCover, t));
                //_mat.SetFloat("_Cutoff", Mathfx.Lerp(0, _screenToCover, t));

                yield return new WaitForEndOfFrame();
            }
            yield return null;
        }

    }

    private IEnumerator ReturnToNormal()
    {
        float elapsed = 0f;
        float time = _scaleTime;
        _returnToNormal = false;
        while ((elapsed < time))
        {
            {
                elapsed += Time.deltaTime * _speed;
                float t = elapsed / time;
                _mat.SetFloat("_Cutoff", Mathfx.Coserp(_screenToCover, 0, t));
                yield return new WaitForEndOfFrame();
            }
            yield return null;
        }
        SetAsFinished();
    }


}
