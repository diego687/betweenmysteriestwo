﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRAMEWORK_GAME;

    public class CEffectManager : MonoBehaviour
    {
        public static CEffectManager Inst;
        private List<CEffect> _effects;
        private GameObject[] _eventObjects;
        // public Transform _canvas;
        //private bool _fadeOutFlag = false;
        private CEffect _currentEffect;

        public bool _returnToNormalView = false;

        void Awake()
        {
            Inst = this;
            _effects = new List<CEffect>();
            DontDestroyOnLoad(this.gameObject);
            _eventObjects = Resources.LoadAll<GameObject>("Effects/");
        }
        private void Start()
        {

        }
        public void AddEvent(EffectType type)
        {
            for (int i = 0; i < _eventObjects.Length; i++)
            {
                if (_eventObjects[i].GetComponent<CEffect>()._type == type)
                {
                    // _canvas.gameObject.SetActive(true);
                    GameObject obj = Instantiate<GameObject>(_eventObjects[i], Vector3.zero, _eventObjects[i].transform.localRotation);
                    _currentEffect = obj.GetComponent<CEffect>();
                    //_fadeOutFlag = false;
                    obj.transform.localPosition = Vector3.zero;
                    _effects.Add(obj.GetComponent<CEffect>());
                    break;
                }
            }
        }

        void Update()
        {
            for (int i = _effects.Count - 1; i >= 0; i--)
            {
                _effects[i].UpdateEffect();
                if (_effects[i].Finished())
                {
                    _effects[i].Cleanup();
                    Destroy(_effects[i].gameObject);
                    _effects.RemoveAt(i);
                }
            }
        }

        public CEffect getCurrentEffect()
        {
            return _currentEffect;
        }
        public void returnToNormalView(EffectType aType)
        {
            for (int i = _effects.Count - 1; i >= 0; i--)
            {
                _effects[i].UpdateEffect();
                if (_effects[i].GetComponent<CEffect>()._type == aType)
                {
                    _effects[i]._end = true;
                }
            }
        }

    }

    public enum EffectType
    {
        UP_AND_DOWN,
        COMIC_VIGNETTING,
        PENCIL_STYLE
    }