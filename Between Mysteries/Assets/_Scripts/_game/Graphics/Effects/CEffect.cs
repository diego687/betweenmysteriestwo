﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CEffect : MonoBehaviour
{
    public EffectType _type;
    protected bool _finished = false;
    public bool _end = false;
    

    //public static int STATE_FADEIN = 0;
    //public static int STATE_HOLD = 1;
    //public static int STATE_FADEOUT = 2;
    private int _state;

    void Awake()
    {
        //  SetState(STATE_FADEIN);
    }

    public virtual void UpdateEffect()
    {

    }

    protected void SetAsFinished()
    {
        _finished = true;
    }

    public virtual bool Finished()
    {
        return _finished;
    }

    public virtual void Cleanup()
    {

        //}
        //public int GetState()
        //{
        //    return _state;
        //}

        //public void SetState(int aState)
        //{
        //    _state = aState;
        //}

    }
}




