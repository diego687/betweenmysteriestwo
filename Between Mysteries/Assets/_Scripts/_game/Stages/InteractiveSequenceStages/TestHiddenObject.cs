﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRAMEWORK_GAME
{
    public class TestHiddenObject : HiddenObjectStage
    {

        void Start()
        {
            
        }

        void Update()
        {
            for (int i = 0; i < _hiddenObjectList.Count; i++)
            {
                Debug.Log("hidden object position; " + _hiddenObjectList[i].transform.position);
                HiddenObject temp = _hiddenObjectList[i];
                if (temp.GetClicked())
                {
                    temp.Lerp(temp.transform.position, Vector3.right * 20, 10);
                    _hiddenObjectList.Remove(temp);
                }                    
            }
        }
    }
}