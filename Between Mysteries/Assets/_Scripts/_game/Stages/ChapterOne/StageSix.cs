﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRAMEWORK_GAME
{
    // Cinematic Scene. No interaction. Stephen's front door
    public class StageSix : Stage
    {
        public SpriteRenderer _background;
        [Header("Stage Buttons")]
        public UnityEngine.UI.Button _getRest;
        public UnityEngine.UI.Button _readNewspaper;
        public UnityEngine.UI.Button _checkPhone;

        public override void Initialize()
        {
            base.Initialize();            
            StartCoroutine(StageCoroutine());
        }

        IEnumerator StageCoroutine()
        {
            Game.Inst.FadeTransitionToBlack(false);
            while (Game.Inst.FadingToBlack() != null)
                yield return null;


            Game.Inst.FadeTransitionToBlack(true);
            while (Game.Inst.FadingToBlack() != null)
                yield return null;

            SetStageFinished();
        }
    }
}