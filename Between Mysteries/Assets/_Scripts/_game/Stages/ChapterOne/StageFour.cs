﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRAMEWORK_GAME;

namespace FRAMEWORK_GAME
{
    public class StageFour : Stage
    {
        public SpriteRenderer _background;
        [Header("Stage Buttons")]
        public UnityEngine.UI.Button _readMessageButton;
        public UnityEngine.UI.Button _ignoreMessageButton;

        public override void Initialize()
        {
            base.Initialize();
            _background.color = _background.color.withAlpha(1);
            _readMessageButton.gameObject.SetActive(false);
            _ignoreMessageButton.gameObject.SetActive(false);
            StartCoroutine(StageFourCoroutine());
        }

        public override void OnFirstButtonClicked()
        {
            base.OnFirstButtonClicked();
            Debug.Log("text finished and the scene is over");
            SetNextStage(_stageData._outcomeOne.name);
        }

        IEnumerator StageFourCoroutine()
        {
            Game.Inst.FadeTransitionToBlack(false);
            while (Game.Inst.FadingToBlack() != null)
                yield return null;

            yield return new WaitForSeconds(1);

            _readMessageButton.gameObject.SetActive(true);
            _ignoreMessageButton.gameObject.SetActive(true);

            while (_waitingForInput)
                yield return null;

            yield return new WaitForSeconds(0.5f);
            GetTransitionManager().fadeScreenToBlack(true);
            SetStageFinished();
        }
    }
}