﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRAMEWORK_GAME
{
    public class StageOne : Stage
    {
        public SpriteRenderer _city;
        public float _cityFadeTime = 1;
        public float _timeDisplayingCity = 1;
        public SpriteRenderer _apartment;
        public float _apartmentFadeTime = 1;
        public float _timeDisplayingApartment;

        void Start()
        {
            CEffectManager.Inst.AddEvent(EffectType.UP_AND_DOWN);
        }

        void Update()
        {

        }

        public override void Initialize()
        {
            base.Initialize();
            Debug.Log("initializing stageOne");
            _city.color = _city.color.withAlpha(0);
            _apartment.color = _apartment.color.withAlpha(0);
            StartCoroutine(StageOneCoroutine());
        }

        public override void DestroyStage()
        {
            base.DestroyStage();
        }

        /// <summary>
        /// Starts in black, Fades Background in, waits time and the stageEnds
        /// </summary>
        /// <returns></returns>
        IEnumerator StageOneCoroutine()
        {
            //Debug.Log("stage One coroutine");

            FadeSprite(_city, true, _cityFadeTime);
            while (_fadingSpriteCoroutine != null)
                yield return null;
            yield return new WaitForSeconds(_timeDisplayingCity);

            FadeSprite(_apartment, true, _apartmentFadeTime);
            while (_fadingSpriteCoroutine != null)
                yield return null;
            yield return new WaitForSeconds(_timeDisplayingApartment);

            // Sounds - Audio
            //AudioSource footSteps = GetAudioManager().PlaySFX("FootSteps", false);
            //while (footSteps.isPlaying)
            //    yield return null;
            //Game.Inst.FadeTransitionToBlack(true);
            CSceneLoader.Inst.loadStage(TransitionType.LEFT_TO_RIGHT);
            while (CTransitionsManager.Inst.getCurrentTransition()!= null && CTransitionsManager.Inst.getCurrentTransition().GetState()!= 2)
                yield return null;
            SetStageFinished();
            //Debug.Log("Stage one finished about to spawn a new stage with dictionary");
        }
    }
}