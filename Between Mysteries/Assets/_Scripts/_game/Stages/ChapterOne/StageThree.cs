﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRAMEWORK_API;

// TODO: Put everything in the buttons methods inside the stage coroutine
// maybe we should use the buttons on the stage that interact with objects as buttons with no unity actions escept for those that takes as to another stage
namespace FRAMEWORK_GAME
{
    public class StageThree : Stage
    {
        // Public
        public SpriteRenderer _livingRoom;
        public float _livingRoomDisplayTime = 1;
        public Image _cellphonebutttonImage;
        //public SpriteRenderer _cellphone;
        
        public SpriteRenderer _newspaperBackground;
        public SpriteRenderer _chatDisplay;
        public float _cellphoneFadeTime = 0.5f;
        public float _cellphoneShakeTime = 2;
        [Header("Stage Buttons")]
        public UnityEngine.UI.Button _cellphoneButton;
        public UnityEngine.UI.Button _newspaperButton;
        public UnityEngine.UI.Button _foldNewspaperButton;
        public Button _goToStephensButton;
        public Button _getRestButton;

        // Private
        bool _waitForChoice;

        //Private
        //ChapterOne _currentChapter;

        // Use this for initialization
        void Start()
        {
            //_currentChapter = Game.Inst.GetActiveChapter().GetComponent<ChapterOne>();
            CEffectManager.Inst.returnToNormalView(EffectType.UP_AND_DOWN);
        }

        // Update is called once per frame
        void Update()
        {

        }

        // Cell phone
        public override void OnFirstButtonClicked()
        {
            base.OnFirstButtonClicked();
            Debug.Log("open cellphone");
            //SetNextStage(_stageData._outcomeOne.name);
            FadeSprite(_chatDisplay, true, .2f);
            _cellphoneButton.interactable = false;
            _newspaperButton.gameObject.SetActive(false);
            _goToStephensButton.gameObject.SetActive(true);
            _getRestButton.gameObject.SetActive(true);
        }

        // Newspaper
        public override void OnSecondButtonClicked()
        {
            base.OnSecondButtonClicked();
            Debug.Log("Newspaper button");
            //SetNextStage(_stageData._outcomeTwo.name);
            FadeSprite(_newspaperBackground, true, .2f);
            _foldNewspaperButton.gameObject.SetActive(true);
            _cellphoneButton.gameObject.SetActive(false);
            _newspaperButton.gameObject.SetActive(false);
        }

        // Fold newspaper
        public override void OnThirdButtonClicked()
        {
            base.OnThirdButtonClicked();
            Debug.Log("Fold newspaper");
            _foldNewspaperButton.gameObject.SetActive(false);
            FadeSprite(_newspaperBackground, false, .2f);
            _cellphoneButton.gameObject.SetActive(true);
            _newspaperButton.gameObject.SetActive(true);
        }

        // get rest
        public override void OnFourthButtonClicked()
        {
            base.OnFourthButtonClicked();
            _waitForChoice = false;
            SetNextStage(_stageData._outcomeOne.name);
        }

        // Go to stephen's
        public override void OnFifthButtonClicked()
        {
            base.OnFifthButtonClicked();
            _waitForChoice = false;
            SetNextStage(_stageData._outcomeTwo.name);
        }

        public override void Initialize()
        {
            base.Initialize();
            _waitForChoice = true;
            //_waitingForInput = true;
            Debug.Log("Init Stage 3");
            //_newspaperBackground.gameObject.SetActive(false);
            _livingRoom.color = _livingRoom.color.withAlpha(1);
            _cellphonebutttonImage.color = _cellphonebutttonImage.color.withAlpha(0);
            //_cellphoneButton.gameObject.SetActive(false);
            _cellphoneButton.interactable = false;
            //_newspaperButton.gameObject.SetActive(false);
            _newspaperButton.interactable = false;
            _foldNewspaperButton.gameObject.SetActive(false);
            _goToStephensButton.gameObject.SetActive(false);
            _getRestButton.gameObject.SetActive(false);
            _chatDisplay.color = _chatDisplay.color.withAlpha(0);
            StartCoroutine(StageThreeCoroutine());
        }

        // This coroutine should do:
        // -  1) Fade from black
        // -  2) Fade cellphone image
        // -  3) Shake cellphone while playing cellphone SFX
        // -  4) Enable interaction cellphone and newspaper buttons
        // -  5) newspaper button clicked --> activates newspaper image over everything in the scene
        // -  5) b) Fold newspaper button appears
        // -  5) c) when Fold button gets clicked, the newspaper fades and stage 3 appears
        // -  6) cellphone button gets clicked
        // -  7) chat image appears. Options for checking chat with three other people
        //   7) a) chat with woman
        //   7) b) chat with a man
        // -  7) c) message from stephen ---> a) go to sleep
        //                                 b) go to stephen's 

        IEnumerator StageThreeCoroutine()
        {
            // fade to stage
            GetTransitionManager().fadeScreenToBlack(false);
            while (FadingToBlack() != null)
                yield return null;

            FadeImage(_cellphonebutttonImage, true, _cellphoneFadeTime);
            while (_fadingImageCoroutine != null)
                yield return null;

            // shake cellphone sprite
            //_shakeManager.AddShake(.1f, 2);
            //while (_shakeManager._shakeDone == false)
            //{
            //    _cellphone.transform.position += _shakeManager.SmoothShake(2);
            //    yield return null;
            //}

            // Buttons appears
            //_cellphoneButton.gameObject.SetActive(true);
           // _newspaperButton.gameObject.SetActive(true);

            Game.Inst.ShakeGameObject(_cellphoneButton.gameObject, 0.03f, 2, 3);
            while (Game.Inst.Shaking() != null)
                yield return null;

            _cellphoneButton.interactable = true;
            _newspaperButton.interactable = true;

            // Waiting for user input on specific buttons of the scene. Either cellphone or newspaper will trigger a new sceene, any other interaction will trigger text descriptions
            while (_waitForChoice)
                yield return null;

            // fade to black and finish stage
            //GetTransitionManager().fadeScreenToBlack(true);
            //while (FadingToBlack() != null)
            //    yield return null;
            CSceneLoader.Inst.loadStage(TransitionType.UP_AND_DOWN);
            while (CTransitionsManager.Inst.getCurrentTransition() != null && CTransitionsManager.Inst.getCurrentTransition().GetState() != 2) // TODO: change int state to enum so we can read the name of the state
                yield return null;
            SetStageFinished();
        }
    }
}