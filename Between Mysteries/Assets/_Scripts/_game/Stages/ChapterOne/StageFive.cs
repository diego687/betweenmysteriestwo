﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRAMEWORK_GAME
{
    public class StageFive : Stage
    {
        public SpriteRenderer _background;
        [Header("Stage Buttons")]
        public UnityEngine.UI.Button _goToSleep;
        public UnityEngine.UI.Button _pourDrink;

        public override void Initialize()
        {
            base.Initialize();
            StartCoroutine(StageCoroutine());
        }

        IEnumerator StageCoroutine()
        {
            Game.Inst.FadeTransitionToBlack(false);
            while (Game.Inst.FadingToBlack() != null)
                yield return null;

            SetStageFinished();
        }
    }
}