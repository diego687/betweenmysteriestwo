﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRAMEWORK_GAME
{
    // Isabella inside Stephen's house. Has to make a choice: baby, stephen's body or get out
    public class StageSeven : Stage
    {
        // Baby - outcome stage 8
        public override void OnFirstButtonClicked()
        {
            base.OnFirstButtonClicked();
        }

        // stephen's body - outcome stage 9
        public override void OnSecondButtonClicked()
        {
            base.OnSecondButtonClicked();
        }

        // Get out of the house - outcome stage 10
        public override void OnThirdButtonClicked()
        {
            base.OnThirdButtonClicked();
        }

        public override void Initialize()
        {
            base.Initialize();
            StartCoroutine(StageCoroutine());
        }

        IEnumerator StageCoroutine()
        {
            while (_waitingForInput)            
                yield return null;
            SetStageFinished();
        }
    }
}