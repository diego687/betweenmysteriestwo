﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRAMEWORK_GAME
{
    public class StageTwo : Stage
    {
        public SpriteRenderer _apartmentHall;
        public float _timeDisplayingApartment = 2;
        public SpriteRenderer _newspaperTable;
        public float _newspaperFadeTime = 0.5f;
        public float _timeDisplayingNewspaperTable = 2;

        void Start()
        {

        }

        void Update()
        {

        }

        public override void Initialize()
        {
            base.Initialize();
            //Debug.Log("initializing stage two");
            _apartmentHall.color = _apartmentHall.color.withAlpha(1); // fades when transition finished
            _newspaperTable.color = _newspaperTable.color.withAlpha(0);
            StartCoroutine(StageTwoCoroutine());
        }

        IEnumerator StageTwoCoroutine()
        {
            Game.Inst.FadeTransitionToBlack(false);
            while (Game.Inst.FadingToBlack() != null)
                yield return null;
            // Display apartment hall 
            yield return new WaitForSeconds(_timeDisplayingApartment);
            FadeSprite(_newspaperTable, true, _newspaperFadeTime);
            while (_fadingSpriteCoroutine != null)
                yield return null;
            CEffectManager.Inst.AddEvent(EffectType.COMIC_VIGNETTING);
            yield return new WaitForSeconds(_timeDisplayingNewspaperTable);
            //GetTransitionManager().fadeScreenToBlack(true);
            CSceneLoader.Inst.loadStage(TransitionType.DIAGONAL_OUTIN);
            while (CTransitionsManager.Inst.getCurrentTransition() != null && CTransitionsManager.Inst.getCurrentTransition().GetState() != 2)
                yield return null;
            CEffectManager.Inst.returnToNormalView(EffectType.COMIC_VIGNETTING);
            SetStageFinished();
        }
    }
}