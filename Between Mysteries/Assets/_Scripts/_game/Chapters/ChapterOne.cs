﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRAMEWORK_GAME;

namespace FRAMEWORK_GAME
{
    public class ChapterOne : Chapter
    {
        public enum WeaponType { Knife, Gun, Count };
        public WeaponType _weapon;

        void Start()
        {

        }

        void Update()
        {

        }

        public void SetWeapon(WeaponType aWeapon)
        {
            _weapon = aWeapon;
        }

        public WeaponType GetWeapon()
        {
            return _weapon;
        }
    }
}