﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRAMEWORK_GAME;
using FRAMEWORK_API;

namespace FRAMEWORK_DEBUG
{
    public class StageTester : MonoBehaviour
    {
        [Header("Stage to test")]
        public Stage _testStage;
        public UnityEngine.UI.Button _testStageButton;
        TransitionManager _transitioManager;

        // Use this for initialization
        void Start()
        {
            _transitioManager = TransitionManager.Inst;
            if (_testStage != null)
                _testStage.transform.position = Vector3.zero;
        }

        public void OnTestClicked()
        {
            Debug.Log("On test clicked");
            if (_testStage == null)
            {
                Debug.Log("First insert a Stage To test, the Test Stage field in the inspector is empty");
                return;
            }

            if (_transitioManager.ScreenBlack())
                _transitioManager.fadeScreenToBlack(false, 0);
            _testStage.transform.position = Vector3.zero; // so it's displayed on the camera
            _testStage.Initialize(); // init the stage we are testing
        }
    }
}