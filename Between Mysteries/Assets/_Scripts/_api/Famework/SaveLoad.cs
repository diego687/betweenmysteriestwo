﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace FRAMEWORK_API
{
    public static class SaveLoad
    {
        public const string EXTENSION = "gbd";

        public static string GetFileName(string aName)
        {
            return Application.persistentDataPath + "/" + aName + "." + EXTENSION;
        }

        public static void Save<T>(T aData, string aSaveFile)
        {
            BinaryFormatter tBf = new BinaryFormatter();
            string tFilePath = GetFileName(aSaveFile);
            FileStream tFile = File.Create(tFilePath);
            tBf.Serialize(tFile, aData);
            tFile.Close();
#if UNITY_EDITOR
            Debug.Log("File '" + tFilePath + "' saved");
#endif
        }

        public static bool Load<T>(ref T aData, string aSaveFile)
        {
            string tFileName = GetFileName(aSaveFile);
            if (File.Exists(tFileName))
            {
                BinaryFormatter tBf = new BinaryFormatter();
                FileStream tFile = File.Open(tFileName, FileMode.Open);
                aData = (T)tBf.Deserialize(tFile);
                tFile.Close();

#if UNITY_EDITOR
                Debug.Log("File '" + tFileName + "' loaded");
#endif
                return true;

            }
            else
            {
#if UNITY_EDITOR

                Debug.Log("The file '" + tFileName + "' does not exist");

#endif
                return false;
            }

        }
    }

}