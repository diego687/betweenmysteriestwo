﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRAMEWORK_API
{
    public class SteeredGameObject : MonoBehaviour
    {
        public enum SteeringState {Wander, Idle, ManualWander, WanderInArea, Count};
        public SteeringState _currentSteeringState;

        private Vector3 _vel;
        private int _state;
        private float _timeInState = 0;
        public float _speed = 2;
        public bool _drawGizmos = false;

        [Header("Wander variables")]
        public float _wanderRadius = 2;
        //private Vector3 _wanderOrigin;
        private float _lastWanderTime = 0;
        public float _wanderInterval = 0;
        private Vector3 _wanderTarget;
        private Vector3 _manualTargetPosition;
        [Range(0, 1f)]
        public float _wanderAngle = .374f; //0.374
        [Range(0, 1f)]
        public float _wanderDirection = 0.25f; //0.25
        public float _wanderRange = 2f; //2
        private Vector4 _targetWanderArea;

        // Use this for initialization
        void Awake()
        {
            //_wanderOrigin = transform.position;
        }

        void Start()
        {

        }

        protected void SetSteeringState(SteeringState aSteeringState)
        {
            _currentSteeringState = aSteeringState;
            if (_currentSteeringState == SteeringState.Wander)
            {
                _lastWanderTime = Time.time;
                ChooseWanderDest();
            }

            else if (_currentSteeringState == SteeringState.Idle)
            {
                _vel = Vector3.zero;
                //_wanderTarget = transform.position; // this will reset the wnaderPosition
                ResetWanderPosition();
            }

            else if (_currentSteeringState == SteeringState.ManualWander)
            {
                _vel = Vector3.zero;
                ResetWanderPosition();
            }

            else if (_currentSteeringState == SteeringState.WanderInArea)
            {
                _lastWanderTime = Time.time;
                ChooseWanderDestinationInArea(_targetWanderArea);
            }
        }

        protected SteeringState GetSteeringState()
        {
            return _currentSteeringState;
        }

        public virtual void CustomUpdate()
        {
            transform.position += _vel * Time.deltaTime;
            _timeInState += Time.deltaTime;

            if (_currentSteeringState == SteeringState.Wander)
            {
                Rotate();
                Vector3 toTargetVector = _wanderTarget - transform.position;
                if (toTargetVector.magnitude < .1f)
                {
                    if (_vel.sqrMagnitude != 0)
                        _lastWanderTime = Time.time;

                    _vel = Vector3.zero;

                    if (Time.time - _lastWanderTime >= _wanderInterval)
                    {
                        _lastWanderTime = Time.time;

                        ChooseWanderDest();
                    }
                }

                else
                {
                    _vel = toTargetVector.normalized * _speed;
                }
            }

            else if (_currentSteeringState == SteeringState.ManualWander)
            {
                RotateToFacePoint(_manualTargetPosition);
                Vector3 toManualWanderPos = _manualTargetPosition - transform.position;
                if (toManualWanderPos.magnitude < 0.1f)
                {
                    SetSteeringState(SteeringState.Wander);
                    return;
                }

                else
                {
                    _vel = toManualWanderPos.normalized * _speed;
                }
            }

            else if (_currentSteeringState == SteeringState.WanderInArea)
            {
                Rotate();
                Vector3 toTargetVector = _wanderTarget - transform.position;
                if (toTargetVector.magnitude < .1f)
                {
                    if (_vel.sqrMagnitude != 0)
                        _lastWanderTime = Time.time;

                    _vel = Vector3.zero;

                    if (Time.time - _lastWanderTime >= _wanderInterval)
                    {
                        _lastWanderTime = Time.time;
                        ChooseWanderDestinationInArea(_targetWanderArea);
                    }
                }

                else
                {
                    _vel = toTargetVector.normalized * _speed;
                }
            }
        }

        public virtual void Update()
        {
            //transform.position += _vel * Time.deltaTime;
            //Rotate();
            //_timeInState += Time.deltaTime;

            //if (_currentSteeringState == SteeringState.Wander)
            //{
            //    Vector3 toTargetVector = _wanderTarget - transform.position;
            //    if (toTargetVector.magnitude < .1f)
            //    {
            //        if (_vel.sqrMagnitude != 0)
            //            _lastWanderTime = Time.time;

            //        _vel = Vector3.zero;

            //        if (Time.time - _lastWanderTime >= _wanderInterval)
            //        {
            //            _lastWanderTime = Time.time;

            //            ChooseWanderDest();
            //        }
            //    }

            //    else
            //    {
            //        _vel = toTargetVector.normalized * _speed;
            //    }
            //}
        }

        private void ResetWanderPosition()
        {
            _wanderTarget = transform.position;
        }

        private void ChooseWanderDest()
        {
            Vector3 direction = rotateVectorWithDot(transform.right, _wanderDirection) * _wanderRange; // change transform.right to facingDirection ?
                                                                                                       //Vector3 lowerOffset = rotateVectorWithDot(direction, _wanderAngle / 2f);
                                                                                                       //Vector3 upperOffset = rotateVectorWithDot(direction, -_wanderAngle / 2f);        

            Vector3 dir = rotateVectorWithDot(direction, Random.Range(-_wanderAngle, _wanderAngle) / 2f);
            _wanderTarget = (transform.position + direction * _wanderRadius) + dir * _wanderRadius;
        }

        private void ChooseWanderDestinationInArea(float aTop, float aBottom, float aLeft, float aRight)
        {
            float randomPointY = Random.Range(aBottom, aTop);
            float randomPointX = Random.Range(aLeft, aRight);
            _wanderTarget = new Vector3(randomPointX, randomPointY);
        }

        private void ChooseWanderDestinationInArea(Vector4 aArea)
        {
           // Debug.Assert(aArea == null, "An Area must be passed as a parameter");
            float randomPointY = Random.Range(aArea.x, aArea.y);
            float randomPointX = Random.Range(aArea.z, aArea.w);
            _wanderTarget = new Vector3(randomPointX, randomPointY);
        }

        public void SetWanderArea(Vector4 aArea)
        {
            _targetWanderArea = aArea;
        }

        public void SetManualWanderTarget(Vector3 aWanderPosition)
        {
            _manualTargetPosition = aWanderPosition;
        }

        // Note: if the sprite animation is NOT top down, this should be removed
        private void Rotate()
        {
            if (_vel.magnitude > 0 && _vel.normalized != Vector3.zero)
            {
                Vector3 vectorToTarget = _wanderTarget - transform.position;
                float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg - 90;
                Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 1);
            }
        }

        private void RotateToFacePoint(Vector3 aPoint)
        {
            if (_vel.magnitude > 0 && _vel.normalized != Vector3.zero)
            {
                Vector3 vectorToTarget = aPoint - transform.position;
                float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg - 90;
                Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 1);
            }
        }

        private void OnDrawGizmos()
        {
            if (!_drawGizmos)
                return;
            Gizmos.color = new Color(0, 1, 1, .3f);
            Gizmos.DrawSphere(transform.position, _wanderRadius);

            Gizmos.color = new Color(1, 1, 0, .6f);
            Gizmos.DrawSphere(_wanderTarget, .2f);

            Gizmos.color = new Color(1, 0, 0, .8f);
            Vector3 facingDirection = rotateVectorWithDot(transform.right, _wanderDirection) * _wanderRange;
            Gizmos.DrawRay(transform.position, facingDirection);
            Gizmos.DrawRay(transform.position, rotateVectorWithDot(facingDirection, -_wanderAngle / 2f));
            Gizmos.DrawRay(transform.position, rotateVectorWithDot(facingDirection, _wanderAngle / 2f));
        }

        private Vector3 rotateVectorWithDot(Vector3 dir, float d)
        {
            float a = Mathf.Atan2(dir.y, dir.x) + (d * 360f);
            float d2r = Mathf.Deg2Rad;
            return new Vector3(
                dir.x * Mathf.Cos(a * d2r) - dir.y * Mathf.Sin(a * d2r),
                dir.y * Mathf.Cos(a * d2r) + dir.x * Mathf.Sin(a * d2r),
                0);
        }
    }
}