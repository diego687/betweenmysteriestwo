﻿using UnityEngine;
using System.Collections;

namespace FRAMEWORK_API
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        static T _inst;
        public static T Inst
        {
            get
            {
                if (_inst == null)
                {

                    //create a new object 
                    GameObject obj = new GameObject();

                    //add the componet needed
                    T comp = obj.AddComponent<T>();

                    //get the name of the class to set it as the new object's name
                    string name = comp.GetType().ToString();
                    //Debug.Log(name);
                    //name = name.Trim("C"[0]);
                    //Debug.Log(name);
                    obj.name = name;

                    //return the new component, if it's not the only one it will destroy itself at Awake 
                    return comp;
                }
                return _inst;
            }
        }

        protected virtual void Awake()
        {
            if (_inst != null && _inst != this)
            {
                Destroy(gameObject);
                return;
            }
            _inst = this as T;
        }
    }
}
