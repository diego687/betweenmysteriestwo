﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;

namespace FRAMEWORK_API
{
    public class AudioManager : MonoBehaviour
    {

        #region SINGLETON
        private static AudioManager _inst;
        public static AudioManager Inst
        {
            get
            {
                if (_inst == null)
                {
                    _inst = FindObjectOfType(typeof(AudioManager)) as AudioManager;
                    if (_inst == null)
                    {
                        GameObject obj = new GameObject("AudioManager");
                        _inst = obj.AddComponent<AudioManager>();
                        //_inst.Init();
                        //DontDestroyOnLoad(obj);
                    }
                    //return _inst;
                    //else
                    //{
                    //    _inst.Init();
                    //}
                }
                return _inst;
            }
        }
        #endregion

        public float maxHearingDistance = 30f; //max distance in meters in which the sound dies off.

        #region EDITOR VARIABLES
        //public List<CAudioFile> musicAssetList;
        //public List<CAudioFile> sfxAssetList;
        #endregion

        #region PRIVATE VARIABLES
        private Dictionary<string, AudioFile> musicList;
        private Dictionary<string, AudioFile> sfxList;
        private AudioSource activeMusicAudioSource;
        private AudioSource activeSecondaryMusicAudioSource;
        private List<AudioSource> activeSFXAudioSources;
        private string activeMusicAudioHash;
        private string activeSecondaryMusicAudioHash = "";
        private bool sfxPaused = false;
        #endregion

        #region VOLUMES
        [SerializeField]
        private float musicVolume = 1f;
        [SerializeField]
        private float sfxVolume = 1f;
        #endregion

        public AudioMixerGroup _musicMixerGroup;
        public AudioMixerGroup _musicSecondaryMixerGroup;
        public AudioMixerGroup _sfxMixerGroup;
        public AudioMixerGroup _vocalFxMixerGroup;

        public AudioMixerSnapshot _mainSnap;
        public AudioMixerSnapshot _secondarySnap;
        public AudioMixerSnapshot _slowMotionSnapShot;
        public float _toSecondaryTransitionTime = 0;
        public float _toMainTransitionTime = 1;
        public float _toSlomoTime = .2f;

        public delegate void LogAudioDelegate(string audioName, int type, bool loops, float timeStamp);
        private LogAudioDelegate OnLogAudio;
        private bool _isLoggingAudio = false;

        public void SubscribeOnLogAudio(LogAudioDelegate action)
        {
            OnLogAudio += action;
        }

        public void UnSubscribeOnLogAudio(LogAudioDelegate action)
        {
            OnLogAudio -= action;
        }

        public void EnableAudioLogging()
        {
            _isLoggingAudio = true;
        }

        public void DisableAudioLogging()
        {
            _isLoggingAudio = false;
        }

        private void Awake()
        {
            if (_inst != null)// && _inst != this)
            {
                Destroy(this.gameObject);
                return;
            }
            _inst = this;
            DontDestroyOnLoad(gameObject);
            //TODO: load volume & music fx from save when implemented
            //if (musicAssetList == null)
            //    musicAssetList = new List<CAudioFile>();

            //if (sfxAssetList == null)
            //    sfxAssetList = new List<CAudioFile>();

            //if (musicAssetList.Count == 0)
            //    Debug.LogWarning("No Music added in CAudioManager. Playing a clip will resort in Exception!");
            //if (sfxAssetList.Count == 0)
            //    Debug.LogWarning("No SFX added in CAudioManager. Playing a clip will resort in Exception!");

            //creates music audio source
            BuildMusicSource();
            activeSFXAudioSources = new List<AudioSource>();

            //creates music&sfx audio reference sheet
            musicList = new Dictionary<string, AudioFile>();
            sfxList = new Dictionary<string, AudioFile>();

            AudioFile[] assetList = Resources.LoadAll<AudioFile>("Data/Audio/");

            for (int i = 0; i < assetList.Length; i++)
            {
                if (assetList[i]._type == AudioType.Music)
                    musicList.Add(assetList[i].name, assetList[i]);
                else if (assetList[i]._type == AudioType.SFX && assetList[i]._enabled)
                    sfxList.Add(assetList[i].name, assetList[i]);
                //    musicList.Add(musicAssetList[i].hash, musicAssetList[i].source);
            }

            //for (int j = 0; j < sfxAssetList.Count; j++)
            //{
            //    sfxList.Add(sfxAssetList[j].hash, sfxAssetList[j].source);
            //}
            //SetMasterVolume(CSaveDataUtility.LoadMasterVolume());
            //SetMusicVolume(CSaveDataUtility.LoadMusicVolume());
            //SetSFXVolume(CSaveDataUtility.LoadSFXVolume());
        }

        public static bool Exists()
        {
            return _inst != null;
        }

        private void BuildMusicSource()
        {
            GameObject source = new GameObject("MusicSource"); //create new object
                                                               //AudioListener listener = GameObject.FindObjectOfType<AudioListener>() as AudioListener; //gets listener

            //source.transform.parent = listener.gameObject.transform; //set the audiolistener object as parent
            source.transform.localPosition = Vector3.zero; //set position to middle of object
            activeMusicAudioSource = source.AddComponent<AudioSource>(); //adds an audiosource & saves reference
            activeMusicAudioSource.outputAudioMixerGroup = _musicMixerGroup;
            DontDestroyOnLoad(source);
            //SetMusicVolume(musicVolume);
        }

        private void BuildSecondaryMusicSource()
        {
            GameObject source = new GameObject("SecondaryMusicSource"); //create new object
                                                                        //AudioListener listener = GameObject.FindObjectOfType<AudioListener>() as AudioListener; //gets listener

            //source.transform.parent = listener.gameObject.transform; //set the audiolistener object as parent
            source.transform.localPosition = Vector3.zero; //set position to middle of object
            activeSecondaryMusicAudioSource = source.AddComponent<AudioSource>(); //adds an audiosource & saves reference
            activeSecondaryMusicAudioSource.outputAudioMixerGroup = _musicSecondaryMixerGroup;
            activeSecondaryMusicAudioSource.volume = musicVolume;
            DontDestroyOnLoad(source);
            //SetMusicVolume(musicVolume);
        }

        void LateUpdate()
        {
            //Debug.Log(activeSFXAudioSources);
            for (int i = activeSFXAudioSources.Count - 1; i >= 0; i--)
            {
                AudioSource auxSource = activeSFXAudioSources[i];
                if (auxSource == null || (!auxSource.loop && !auxSource.isPlaying)) //sfx has ended and not looping? or was deleted?
                {
                    if (auxSource != null)
                    {
                        auxSource.Stop();
                        Destroy(auxSource.gameObject); //TODO: once a pool is made, remove gameobject instead of destroying
                    }
                    activeSFXAudioSources.RemoveAt(i);
                }
            }
        }

        public bool IsMusicPlaying(string hash)
        {
            return activeMusicAudioHash == hash;
        }

        public void PlayMusic(string hash, bool loop = true, Vector3 position = default(Vector3))
        {
            if (activeMusicAudioSource == null)
                BuildMusicSource();

            if (activeMusicAudioSource.clip == musicList[hash].source)
                return;
            //get music object, set the audioclip to it and save reference
            AudioFile audioToPlay = musicList[hash];
            activeMusicAudioSource.GetComponent<AudioSource>().Stop();
            activeMusicAudioSource.clip = audioToPlay.source;
            activeMusicAudioSource.priority = 0;
            activeMusicAudioSource.volume = musicVolume * audioToPlay._relativeVolume;
            activeMusicAudioSource.loop = loop;
            activeMusicAudioSource.maxDistance = maxHearingDistance;
            activeMusicAudioSource.Play();
            activeMusicAudioHash = hash;
            if (_isLoggingAudio)
            {
                if (OnLogAudio != null)
                    OnLogAudio(hash, 0, loop, Time.time);
            }
        }

        /// <summary>
        /// Fades primary theme with secondary theme
        /// </summary>
        /// <param name="hash"></param>
        public void PlaySecondaryMusic(string hash, bool loop = true, Vector3 position = default(Vector3))
        {
            if (activeSecondaryMusicAudioSource == null)
                BuildSecondaryMusicSource();

            //if (activeSecondaryMusicAudioSource.clip == musicList[hash])
            //    return;

            //get music object, set the audioclip to it and save reference
            AudioFile audioToPlay = musicList[hash];
            activeSecondaryMusicAudioSource.GetComponent<AudioSource>().Stop();
            activeSecondaryMusicAudioSource.clip = audioToPlay.source;
            activeSecondaryMusicAudioSource.priority = 0;
            activeMusicAudioSource.volume = musicVolume * audioToPlay._relativeVolume;
            activeSecondaryMusicAudioSource.loop = loop;
            activeSecondaryMusicAudioSource.maxDistance = maxHearingDistance;
            activeSecondaryMusicAudioSource.Play();
            activeSecondaryMusicAudioHash = hash;
            if (_isLoggingAudio)
            {
                if (OnLogAudio != null)
                    OnLogAudio(hash, 2, loop, Time.time);
            }

            _secondarySnap.TransitionTo(_toSecondaryTransitionTime);
        }

        public string GetSecondaryMusicAudioHash()
        {
            return activeSecondaryMusicAudioHash;
        }

        public void TransitionToMainSnapshot()
        {
            _mainSnap.TransitionTo(_toMainTransitionTime);
            StopCoroutine("StopSecondaryMusicIn");
            StartCoroutine("StopSecondaryMusicIn", _toMainTransitionTime);
        }

        public void TransitionToSlomoSnapshot()
        {
            _slowMotionSnapShot.TransitionTo(_toSlomoTime);
        }

        IEnumerator StopSecondaryMusicIn(float time)
        {
            yield return new WaitForSeconds(time);
            if (activeSecondaryMusicAudioSource != null)
                activeSecondaryMusicAudioSource.Stop();
        }

        public AudioSource PlaySFX(string hash, bool loop, Transform parent = null, bool singleInstance = false)
        {
            if (hash == "")
                return null;

            if (singleInstance)
                StopSFX(hash, true);

            if (!sfxList.ContainsKey(hash))
                return null;
            //get new audiosource object, set the clip and properties and save reference
            AudioFile audioToPlay = sfxList[hash];

            GameObject sfxObj = new GameObject(hash); //TODO: once a pool is made, get gameobject from pool instead of creating new
            sfxObj.transform.parent = parent;
            sfxObj.transform.localPosition = Vector3.zero;
            AudioSource source = sfxObj.AddComponent<AudioSource>();
            source.GetComponent<AudioSource>().clip = audioToPlay.source;
            source.volume = audioToPlay._relativeVolume;
            source.loop = loop;
            source.minDistance = maxHearingDistance;
            source.maxDistance = maxHearingDistance;
            source.dopplerLevel = 0f; //sets doppler to 0 to cancel pitch distortions on movement
            source.outputAudioMixerGroup = _sfxMixerGroup;

            source.Play();

            if (_isLoggingAudio)
            {
                if (OnLogAudio != null)
                    OnLogAudio(hash, 1, loop, Time.time);
            }

            activeSFXAudioSources.Add(source);
            return source;
        }

        public float GetMasterVolume()
        {
            return AudioListener.volume;
        }

        public float GetMusicVolume()
        {
            return musicVolume;
        }

        public float GetSFXVolume()
        {
            return sfxVolume;
        }

        public void SetMasterVolume(float aVolume)
        {
            AudioListener.volume = aVolume;
        }

        public void SetMusicVolume(float aVolume)
        {
            musicVolume = aVolume;
            UpdateMusicVolume();
        }

        public void SetSFXVolume(float aVolume)
        {
            sfxVolume = aVolume;
            UpdateSFXVolume();
        }

        private void UpdateMusicVolume()
        {
            if (activeMusicAudioSource != null)
            {
                activeMusicAudioSource.volume = musicVolume;
            }
            if (activeSecondaryMusicAudioSource != null)
                activeSecondaryMusicAudioSource.volume = musicVolume;
        }

        private void UpdateSFXVolume()
        {
            for (int i = 0; i < activeSFXAudioSources.Count; i++)
            {
                activeSFXAudioSources[i].volume = sfxVolume;
            }
        }

        public void TogglePauseAll()
        {
            TogglePauseMusic();
            TogglePauseSFX();
        }

        public void TogglePauseMusic()
        {
            if (activeMusicAudioSource.GetComponent<AudioSource>().isPlaying)
            {
                activeMusicAudioSource.Pause();
            }
            else
            {
                activeMusicAudioSource.Play();
            }

        }

        public void TogglePauseSFX()
        {
            for (int i = 0; i < activeSFXAudioSources.Count; i++)
            {
                if (sfxPaused)
                {
                    activeSFXAudioSources[i].Play();
                }
                else
                {
                    activeSFXAudioSources[i].Pause();
                }
            }

            sfxPaused = !sfxPaused;
        }

        public void ResetCurrentMusic()
        {
            activeMusicAudioSource.GetComponent<AudioSource>().time = 0f;
        }

        public void StopAll()
        {
            StopMusic();
            StopSFX();
        }

        public void StopMusic()
        {
            activeMusicAudioSource.Stop();
        }

        public void StopSFX()
        {
            for (int i = 0; i < activeSFXAudioSources.Count; i++)
            {
                activeSFXAudioSources[i].Stop();
            }
        }

        public void StopSFX(string name, bool all = false)
        {
            for (int i = activeSFXAudioSources.Count - 1; i >= 0; i--)
            {
                if (activeSFXAudioSources[i].gameObject.name == name)
                {
                    activeSFXAudioSources[i].Stop();
                    Destroy(activeSFXAudioSources[i].gameObject);
                    activeSFXAudioSources.RemoveAt(i);
                    if (!all)
                        break;
                }
            }
        }

        public void StopSFX(AudioSource source)
        {
            activeSFXAudioSources.Remove(source);
            Destroy(source.gameObject);
        }
    }
}
