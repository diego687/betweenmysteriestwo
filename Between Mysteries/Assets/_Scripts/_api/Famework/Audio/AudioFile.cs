﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu]
[System.Serializable]
public class AudioFile : ScriptableObject
{
    public AudioType _type;
    public AudioClip source;
    [Range(0, 1f)]
    public float _relativeVolume = 1;
    public bool _enabled = true;
}

public enum AudioType
{
    SFX,
    Music
}
