﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRAMEWORK_API
{
    public interface IState
    {
        void OnEnter();
        void OnUpdate();
        void OnExit();
    }
}