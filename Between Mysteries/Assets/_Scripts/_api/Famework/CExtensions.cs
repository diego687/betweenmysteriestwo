﻿using UnityEngine;

public static class CExtensions
{
    public static Vector3 toX0Y(this Vector3 aVector)
    {
        return new Vector3(aVector.x, 0, aVector.y);
    }

    public static Color withAlpha(this Color aColor, float aAlpha)
    {
        return new Color(aColor.r, aColor.g, aColor.b, aAlpha);
    }

    public static float floatSquared(this float aFloat)
    {
        return aFloat * aFloat;
    }

    public static int intSquared(this int aInt)
    {
        return aInt * aInt;
    }

    public static Vector3 toWorld(this Vector3 aVector, float aZoffset)
    {
        return Camera.main.ScreenToViewportPoint(new Vector3(aVector.x, aVector.y, aZoffset));
    }

    public static Vector3 toWorldFlat(this Vector3 aVector)
    {
        return aVector.toWorld(-Camera.main.transform.position.z);
    }

    public static int toInt(this float aFloat)
    {
        return Mathf.FloorToInt(aFloat);
    }

    public static Vector3 lerpTo(this Vector3 aOrigin, Vector3 aDestination, float aSpeed)
    {
        return Vector3.Lerp(aOrigin, aDestination, aSpeed * Time.deltaTime);
    }

    //public static RectTransform gameObjectToRectTransform(this GameObject aGameObject)
    //{
    //    return aGameObject.transform as RectTransform;
    //}

    //public static RectTransform transformToRectTransform(this Transform aTransform)
    //{
    //    return aTransform as RectTransform;
    //}

    static public RectTransform rectTransform(this GameObject self)
    {
        return self.transform as RectTransform;
    }

    static public RectTransform rectTransform(this Transform self)
    {
        return self as RectTransform;
    }

    static public RectTransform rectTransform(this MonoBehaviour self)
    {
        return self.transform as RectTransform;
    }

    //public static RectTransform monobehaviourToRectTransform(this MonoBehaviour aMonobehaviour)
    //{
    //    return aMonobehaviour.transform as RectTransform;
    //}
}
