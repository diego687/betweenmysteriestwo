﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRAMEWORK_API
{
    public class GameObjectPool : Singleton<GameObjectPool>
    {
        List<GameObject> _openList;
        List<GameObject> _closedList;

        protected override void Awake()
        {
            base.Awake();
            _openList = new List<GameObject>();
            _closedList = new List<GameObject>();
        }

        public void AddBatch(int aQuantity)
        {
            for (int i = 0; i < aQuantity; i++)
            {
                GameObject tempObject = new GameObject("SpritePool GameObject");
                tempObject.SetActive(false);
                tempObject.transform.SetParent(transform);
                _openList.Add(tempObject);
            }
        }

        public GameObject GrabFirstAvailableGameObject()
        {
            if (_openList.Count == 0)
                AddBatch(10);
            GameObject tempObjectToReturn = _openList[0];
            _openList.Remove(tempObjectToReturn);
            tempObjectToReturn.SetActive(true);
            _closedList.Add(tempObjectToReturn);
            return tempObjectToReturn;
        }

        public void GiveBackGameObject(GameObject aGameObject, bool aRemoveGameObjectComponents = true)
        {
            _closedList.Remove(aGameObject);
            aGameObject.SetActive(false);
            if (aRemoveGameObjectComponents)
                RemoveComponents(aGameObject);
            aGameObject.name = "SpritePool GameObject";
            _openList.Add(aGameObject);
        }

        void RemoveComponents(GameObject aGameObject)
        {
            Component[] gameObjectComponents = aGameObject.GetComponents<Component>();
            foreach (Component currentComponent in gameObjectComponents)
            {
                if (currentComponent is Transform)
                    continue;
                GameObject.Destroy(currentComponent);
            }
        }
    }
}