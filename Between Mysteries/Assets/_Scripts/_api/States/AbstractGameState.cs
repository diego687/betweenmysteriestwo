﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRAMEWORK_API;

namespace FRAMEWORK_GAME
{
    public class AbstractGameState : AbstractState
    {

        public override void OnEnter()
        {
            base.OnEnter();
        }

        /// <summary>
        /// The base.OnExit should be at the bottom of the override method.
        /// This enables the state to complete the logic of the state before being removed
        /// </summary>
        public override void OnExit()
        {
            base.OnExit();
        }

        public override void OnUpdate()
        {
            base.OnUpdate();
        }
    }

}