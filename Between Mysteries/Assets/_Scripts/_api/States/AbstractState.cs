﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRAMEWORK_API
{
    public class AbstractState : IState
    {
        public virtual void OnEnter()
        {
            StateManager.AddState(this);
        }

        public virtual void OnUpdate()
        {

        }

        public virtual void OnExit()
        {
            StateManager.removeState(this);
        }
    }
}