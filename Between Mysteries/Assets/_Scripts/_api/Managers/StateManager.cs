﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRAMEWORK_API;

namespace FRAMEWORK_API
{
    // Manager that stores and updates every State. This will hold every State of the game, Level, player, etc.
    public class StateManager : Singleton<StateManager>
    {
        public List<IState> _currentStateList;
        /*
        private static StateManager Inst;
        public static StateManager instance
        {
            get
            {
                if (!Inst)
                {
                    Inst = FindObjectOfType(typeof(StateManager)) as StateManager;
                    if (!Inst)
                    {
                        // TODO: Create a gameObject and add this script as a component
                        Debug.LogError("GameObject with StateManager component missing");
                        return null;
                    }

                    else
                    {
                        Inst.Initialize();
                    }
                }
                return Inst;
            }
        }*/

        protected override void Awake()
        {
            base.Awake();
            Initialize();
        }

        public void Initialize()
        {
            _currentStateList = new List<IState>();
            //Debug.Log("Initialize the State manager");
        }

        public static void AddState(IState aState)
        {
            //Debug.Log("add state to state manager: " + aState.ToString());
            // check if already in the list
            if (!Inst._currentStateList.Contains(aState))
                Inst._currentStateList.Add(aState);
        }

        private void Update()
        {

            if (Inst._currentStateList != null && Inst._currentStateList.Count > 0)
            {
                //Debug.Log("#####################################################");
                //Debug.Log("state manager list count: " + Inst._currentStateList.Count);
                for (int i = 0; i < Inst._currentStateList.Count; i++)
                {
                    //Debug.Log("////////////////// State: " + Inst._currentStateList[i].ToString() + " ////////////////");
                    if (_currentStateList[i] != null)
                        _currentStateList[i].OnUpdate();
                }

                //Debug.Log("#####################################################");
            }
        }

        public static void removeState(IState aState)
        {
            //Debug.Log("removing state: " + aState.ToString());
            Inst._currentStateList.Remove(aState);
        }
    }
}