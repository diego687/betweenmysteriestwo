﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRAMEWORK_API
{
    // TODO: instance this as aprefab, we will need it to fade the splash - opening credits and loading
    // For now it stays like this as it's just for debugging the transition between stages
    public class TransitionManager : MonoBehaviour
    {
        #region SINGLETON
        private static TransitionManager _inst;
        public static TransitionManager Inst
        {
            get
            {
                if (_inst == null)
                {
                    _inst = FindObjectOfType(typeof(TransitionManager)) as TransitionManager;
                    if (_inst == null)
                    {
                        _inst = Instantiate(Resources.Load<TransitionManager>("Prefabs/Managers/TransitionManager"));
                        
                        //_inst.Init();
                        //DontDestroyOnLoad(obj);
                    }
                    //return _inst;
                    //else
                    //{
                    //    _inst.Init();
                    //}
                }
                return _inst;
            }
        }
        #endregion

        public UnityEngine.UI.Image _fadeImage;
        public float _fadeTimer = 0.5f;
        public Coroutine _fadeCoroutine;
        bool _screenFaded;
        public enum States { FadeIn, Holding, FadeOut, Idle, Count }  // TODO
        public States _state;

        IEnumerator fadeScreenToBlackCoroutine(bool aValue, float aTime)
        {
            float startTime = Time.time;
            while (Time.time - startTime < aTime)
            {
                float t = (Time.time - startTime) / aTime;
                _fadeImage.color = new Vector4(0, 0, 0, aValue ? t : 1 - t);
                yield return null;
            }

            _fadeImage.color = new Vector4(0, 0, 0, aValue ? 1 : 0);
            _screenFaded = aValue;
            _fadeCoroutine = null;
        }

        public Coroutine fadeScreenToBlack(bool aValue, float aTime = 0.5f)
        {
            return _fadeCoroutine = StartCoroutine(fadeScreenToBlackCoroutine(aValue, aTime));
        }

        protected void Awake()
        {
            //base.Awake();
            DontDestroyOnLoad(this);
        }

        protected void Start()
        {
            SetState(States.FadeOut);
        }

        public bool ScreenBlack()
        {
            return _screenFaded;
        }

        void SetState(States aState)
        {
            _state = aState;

            if (_state == States.FadeIn)
            {

            }

            else if (_state == States.Holding)
            {

            }

            else if (_state == States.FadeOut)
            {

            }

            else if (_state == States.Idle)
            {

            }
        }

        public States GetState()
        {
            return _state;
        }
    }
}