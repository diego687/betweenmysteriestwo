﻿Shader "Custom/SpriteDegradeSmoke" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MaskColor("MaskColor", Color) =(1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_ColorTex ("_ColorTex", 2D) = "white" {}


		_ShapeTexture ("_ShapeTexture", 2D) = "white" {} //Figures that you can see 
		_TransitionTex ("_TransitionTex", 2D) = "white" {} // Texture in degrade to use as sample
		_Cutoff ("Alpha cutoff", Range (0,1)) = 0

		_Speed("Scroll speed", float) = 1
         _HSpeedR("Scroll speed H Right", float) = 1
         _HSpeedL("Scroll speed H Left", float) = 1
	
	}


		SubShader
	{
		pass
		{
			Blend SrcAlpha OneMinusSrcAlpha //como blendea la transparencia
		Tags
         { 
             "Queue"="Transparent" 
             "IgnoreProjector"="True" 
             "RenderType"="Transparent" 
             "PreviewType"="Plane"
             "CanUseSpriteAtlas"="True"
         }
			Cull Off //que cara se dibuja
			ZWrite Off //escribe al zbuffer o no
			CGPROGRAM

			//PRAGMAS
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				//POSITION, NORMAL, TANGENT, COLOR, TEXCOORD0 ~ TEXCOORD8
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
			};

			struct v2f
			{
				float4 pos : SV_POSITION; // SI O SI
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;


			};
			
			fixed4 _Color;
			sampler2D _ShapeTexture;
			sampler2D _TransitionTex;
			sampler2D _MainTex;
			sampler2D _ColorTex;

			float _Cutoff;
			fixed4 _MaskColor;

			fixed _Speed;
			fixed _HSpeedR;
			fixed _HSpeedL;


			v2f vert (appdata IN)
			{
				v2f OUT;
				OUT.pos = UnityObjectToClipPos(IN.vertex);
				OUT.uv = IN.uv;
				OUT.uv2 = IN.uv2;
				return OUT;
			}

			float Posterize(float v, int steps)
			{
				return round(v*steps)/steps;
			}

			float4 frag (v2f v) : COLOR
			{
				//return col;

			fixed4 transit = tex2D(_TransitionTex,v.uv);
			
			if(transit.b <_Cutoff +.06)
			{
				return tex2D(_ShapeTexture,v.uv) * _Color *0;
			}
			else
			{
					fixed4 rColor = tex2D (_ColorTex,v.uv2 + float2(_HSpeedR,(_Speed)) * _Time.y);
					fixed4 gColor = tex2D (_ColorTex,v.uv2 + float2(_HSpeedL, (-_Speed)) * _Time.y);

					fixed alphaClip = saturate(pow(v.uv2.y,.45));
					fixed4 c;
					//return rColor;
					c.rgb = Posterize( saturate((rColor.r + gColor.g + rColor.g + gColor.r) * alphaClip) ,1);
					clip(c.rgb-0.01);
					return tex2D(_MainTex,v.uv) * _MaskColor;
			}
			}
			ENDCG
		}

	}


			 
            
/*
             o.Albedo = c.rgb * _Color ;
             o.Alpha = _Color.a;
			 */
	Fallback "Diffuse"
}
/*
  Shader "Custom/Water"
 {
     Properties
     {
         _MainTex ("Sprite Texture", 2D) = "white" {}
         _Speed("Scroll speed", float) = 1
         _HSpeedR("Scroll speed H Right", float) = 1
         _HSpeedL("Scroll speed H Left", float) = 1
         _Color ("Tint", Color) = (1,1,1,1)
     }
 
     SubShader
     {
         Tags
         { 
             "Queue"="Transparent" 
             "IgnoreProjector"="True" 
             "RenderType"="Transparent" 
             "PreviewType"="Plane"
             "CanUseSpriteAtlas"="True"
         }
 
         Cull Off
         Lighting Off
         ZWrite Off
         Blend One OneMinusSrcAlpha
 
         CGPROGRAM
         #pragma surface surf Lambert vertex:vert nofog keepalpha
         #pragma multi_compile _ PIXELSNAP_ON
         #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
 
         sampler2D _MainTex;
         fixed4 _Color;
         sampler2D _AlphaTex;
         fixed _Speed;
         fixed _HSpeedR;
         fixed _HSpeedL;
 
         struct Input
         {
             float2 uv_MainTex;
             fixed4 color;
         };
         
         void vert (inout appdata_full v, out Input o)
         {
             
             UNITY_INITIALIZE_OUTPUT(Input, o);
             o.color = v.color * _Color;
         }

         float Posterize(float v, int steps)
			{
				return round(v*steps)/steps;
			}
 
         void surf (Input IN, inout SurfaceOutput o)
         {
             fixed4 rColor = tex2D (_MainTex,IN.uv_MainTex + float2(_HSpeedR,_Speed) * _Time.y);
             fixed4 gColor = tex2D (_MainTex,IN.uv_MainTex + float2(_HSpeedL, _Speed) * _Time.y);
			 
            
             fixed alphaClip = saturate(pow(IN.uv_MainTex.y,.45));

             fixed4 c;
			//c.rgb = alphaClip;
            c.rgb = Posterize( saturate((rColor.r + gColor.g) * alphaClip) ,1);
			//c.rgb = gColor.r + rColor.g;
            clip(c.rgb-0.01);

             o.Albedo = c.rgb * _Color * _Color.a;
             o.Alpha = _Color.a;
         }
         ENDCG
     }
 
 Fallback "Transparent/VertexLit"
 }*/