﻿
Shader "Hidden/Transition"
{
	Properties
	{
		_MainTex ("CameraIn", 2D) = "white" {}
		_ShapeTexture ("_ShapeTexture", 2D) = "white" {}
		_GreyTexture ("_GreyTexture", 2D) = "white" {}
		_Cutoff ("Alpha cutoff", Range (0,1)) = 0
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uv1: TEXCOORD1;
				float4 vertex : SV_POSITION;
				float4 screenPos : TEXCOORD2;
			};
			float4 _MainTex_TexelSize;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.uv1 = v.uv;

					#if UNITY_UV_STARTS_AT_TOP
					if (_MainTex_TexelSize.y < 0)
						o.uv1.y = 1 - o.uv1.y;
					#endif
				o.screenPos = o.vertex;
					return o;
			}
			
			sampler2D _MainTex;
			sampler2D _ShapeTexture;
			sampler2D _GreyTexture;

			float _Cutoff;
			fixed4 _Color;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed2 deformUV = i.uv;
				deformUV.y = 1-deformUV.y;
				fixed2 screenUV = deformUV.xy;

				//screenUV.x *= _MainTex_TexelSize.z/_MainTex_TexelSize.w;// / i.screenPos.w;
				//screenUV *= .5;
				//screenUV.y *= _MainTex_TexelSize.y*_MainTex_TexelSize.w;// / i.screenPos.w;
				fixed4 transit = tex2D(_GreyTexture,screenUV);

				
				//fixed4 _MainCol = tex2D(_MainTex,i.uv);
				if(transit.b <_Cutoff)
				{
				return tex2D(_ShapeTexture,screenUV) * _Color;

				//return tex2D(_ShapeTexture,i.uv)* _Color;
				}
				else
				{
				return tex2D(_MainTex,i.uv);
				}

				/*fixed4 col = tex2D(_MainTex, i.uv);
				// just invert the colors
				col = 1 - col;
				return col;*/
			}
			ENDCG
		}
	}
}
