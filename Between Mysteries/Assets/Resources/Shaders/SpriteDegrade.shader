﻿Shader "Custom/SpriteDegrade" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MaskColor("MaskColor", Color) =(1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}

		//_ShapeTexture ("_ShapeTexture", 2D) = "white" {} //Figures that you can see 
		_TransitionTex ("_TransitionTex", 2D) = "white" {} // Texture in degrade to use as sample
		_Mask ("_Mask", 2D) = "white" {} // Texture in degrade to use as sample
		_Cutoff ("Alpha cutoff", Range (0,1)) = 0
	}
		SubShader
	{
		pass
		{
			Blend SrcAlpha OneMinusSrcAlpha //como blendea la transparencia

			Cull Off //que cara se dibuja
			ZWrite Off //escribe al zbuffer o no
			CGPROGRAM

			//PRAGMAS
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				//POSITION, NORMAL, TANGENT, COLOR, TEXCOORD0 ~ TEXCOORD8
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 pos : SV_POSITION; // SI O SI
				float2 uv : TEXCOORD0;

			};
			
			fixed4 _Color;
			//sampler2D _ShapeTexture;
			sampler2D _TransitionTex;
			sampler2D _MainTex;
			sampler2D _Mask;

			float _Cutoff;
			fixed4 _MaskColor;


			v2f vert (appdata IN)
			{
				v2f OUT;
				OUT.pos = UnityObjectToClipPos(IN.vertex);
				OUT.uv = IN.uv;
				return OUT;
			}


			float4 frag (v2f v) : COLOR
			{
				//return col;

			fixed4 transit = tex2D(_TransitionTex,v.uv);
			fixed mask = tex2D(_Mask,v.uv).r;
			
			if(transit.b >_Cutoff -.0000001)
			{
				return transit * 0; //tex2D(_ShapeTexture,v.uv) * _Color * 0;
			}
			else
			{
				return tex2D(_MainTex,v.uv) * _Color*mask;
			}
			}
			ENDCG
		}

	}

	Fallback "Diffuse"
}
 