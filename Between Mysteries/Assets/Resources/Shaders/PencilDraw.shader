﻿Shader "Hidden/PencilDraw"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_NoiseTex("Noise", 2D) = "white"{}
		PI2 ("PI2", float) = 6.28318530717959
		_Range ("Range", Range (0,16)) = 0
		ANGLENUM ("ANGLENUM", float) = 4
		STEP ("STEP", float) = 2
		MAGIC_GRAD_THRESH ("MAGIC_GRAD_THRESH", float) = .01
		MAGIC_SENSITIVITY ("MAGIC_SENSITIVITY", float) = 10. // 4. ver con otros valores
		MAGIC_COLOR ("MAGIC_COLOR", float) = 0.5//1. ver con otros valores

		// Grayscale mode! This is for if you didn't like drawing with colored pencils as a kid
		//#define GRAYSCALE

		// Here's some magic numbers, and two groups of settings that I think looks really nice. 
		// Feel free to play around with them!

		// Setting group 1:
		/*#define MAGIC_SENSITIVITY     4.
		#define MAGIC_COLOR           1.*/

		// Setting group 2:

	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				//float2 uv2 : TEXCOORD1;

			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				//float2 uv2 : TEXCOORD1;

				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			sampler2D _MainTex;
			sampler2D _NoiseTex;
			float ANGLENUM;
			float PI2;
			float _Range;
			float STEP;
			float MAGIC_GRAD_THRESH;
			float MAGIC_SENSITIVITY;
			float MAGIC_COLOR;

			float4 getCol(float2 uv)
			{


				//float2 uv = -1.0 + 2.0*pos.xy/ _ScreenParams.xy;
				//uv.x *= _ScreenParams.x/ _ScreenParams.y ;

				//float2 uv = i.uv//pos / _ScreenParams.xy;
				//return texture(iChannel0, uv);
				return tex2D(_MainTex, uv);
				
			}

			float getVal(float2 uv)
			{
				float4 c=getCol(uv);
				return dot(c.xyz, float3(0.2126, 0.7152, 0.0722));
			}

			float2 getGrad(float2 pos, float eps)
			{
   				float2 d=float2(eps,0);
				return float2(
					getVal(pos+d.xy)-getVal(pos-d.xy),
					getVal(pos+d.yx)-getVal(pos-d.yx)
				)/eps/2.;
			}

			void pR(inout float2 p, float a) {
				p = cos(a)*p + sin(a)*float2(p.y, -p.x);
			}
			/*float absCircular(float t)
			{
				float a = floor(t + 0.5);
				return mod(abs(a - t), 1.0);
			}*/


			fixed4 frag (v2f i) : SV_Target
			{
				/*fixed4 col = tex2D(_MainTex, i.uv);
				// just invert the colors
				col.rgb = 1 - col.rgb;
				return col;*/
				//void mainImage( out vec4 fragColor, in vec2 fragCoord )
				{   
					float2 pos = i.uv /*fragCoord*/;
					float weight = 1.0;
					//return getCol(pos);
					for (float j = 0.; j < ANGLENUM; j += 1.)
					{
						float2 dir = (1,0);
						pR(dir, j * PI2 / (2. * ANGLENUM));
        
						float2 grad = (-dir.y, dir.x);
        
						for (float i = -_Range; i <= _Range; i += STEP)
						{
							float2 pos2 = pos + normalize(dir)*i;
            
							// video texture wrap can't be set to anything other than clamp  (-_-)
							if (pos2.y < 0. || pos2.x < 0. || pos2.x > _ScreenParams.x || pos2.y > _ScreenParams.y)
								continue;
            
							float2 g = getGrad(pos2, 1.);
							if (length(g) < MAGIC_GRAD_THRESH)
								continue;
            
							weight -= pow(abs(dot(normalize(grad), normalize(g))), MAGIC_SENSITIVITY) / floor((2. * _Range + 1.) / STEP) / ANGLENUM;
						}
					}
    
				#ifndef GRAYSCALE
					fixed4 col = getCol(pos);
				#else
					//fixed4 col = float4(getVal(pos));
					fixed4 col = float4(getVal(tex2D(_MainTex,pos)));


				#endif
    
					//fixed4 background = mix(col, float4 (1,1,1,1), MAGIC_COLOR);
					fixed4 background =  lerp(col, float4(1,1,1,1), MAGIC_COLOR); 
					
					// I couldn't get this to look good, but I guess it's almost obligatory at this point...
					/*float distToLine = absCircular(fragCoord.y / (iResolution.y/8.));
					background = mix(vec4(0.6,0.6,1,1), background, smoothstep(0., 0.03, distToLine));*/
    
    
					// because apparently all shaders need one of these. It's like a law or something.
					//float r = length(pos - _ScreenParams.xy*.5) / _ScreenParams.x;
					//float vign = 1. - r*r*r;
    
					float4 a = tex2D(_NoiseTex, pos/_ScreenParams.xy );
					return a;
					float4 finalColor  = lerp(float4(0,0,0,0), background, weight) + a.xxxx/2.;
					
					//finalColor = getCol(pos);
					return finalColor;
				}
			}
			ENDCG
		}
	}
}
